// Home.tsx
import React, { useEffect, useRef, useState } from 'react';
import getCaiPiaoHistory from '../net/getCaiPiaoHistory'

const dataList = []
const currentPage = 1
const currentPageNum = 50
const isInit = false // 由于严格模式 所以useEffect会执行两次 这里需要拦截多余的请求
function Home() {
  const [message, setMessage] = useState('Home component')
  const dataListRef = useRef(dataList)
  const currentPageRef = useRef(currentPage)
  const isInitRef = useRef(isInit)
  function getData() {
    try {
      getCaiPiaoHistory(currentPageRef.current, currentPageNum).then((result) => {
        console.log(`zdy---result--->${result}`)
        if (!result) {
          setMessage('数据为空 1')
          return
        }
        let str = JSON.stringify(result)
        setMessage(str)
        if (200 != result.code && "200" != result.code) {
          setMessage(result.message)
          return
        }
        let resultData = result.data
        if (!resultData) {
          setMessage('数据为空 2')
          return
        }
        // let arrayData = JSON.parse(str)
        if (!(resultData instanceof Array) || resultData.length < 1) {
          setMessage('数据为空 3')
          return
        }
        if (dataListRef.current.length > 0) {
          let lastItem = dataListRef.current[dataListRef.current.length - 1]
          if (lastItem && lastItem.type == 'bottomItem') {
            dataListRef.current.splice(dataListRef.current.length - 1, 1)
          }
        }
        for (let i = 0; i < resultData.length; i++) {
          let child = resultData[i]
          if (!child) {
            continue
          }
          let lottery_res = child.lottery_res
          if (typeof lottery_res != 'string' || lottery_res.length < 1) {
            continue
          }
          let childArr = lottery_res.split(',')
          child.numberList = childArr
          child.type = 'dataItem'
          // console.log(`zdy---child--->${JSON.stringify(child)}`)
          // parentNumList.push(childArr)
          dataListRef.current.push(child)
          if (i == (resultData.length - 1)) {
            dataListRef.current.push({ type: "bottomItem", text: "加载更多" })
          }
        }
        console.log(`zdy---数据总长度--->${dataListRef.current.length},dataList--->${dataList.length}`);
      }).catch((err) => {
        setMessage(err.message)
      })

    } catch (err) {
      setMessage(err.message)
    }
  }
  useEffect(() => {
    if (isInitRef.current == true) {
      return
    }
    console.log(`zdy---进入useEffect--->`);
    isInitRef.current = true
    getData()
  }, [])

  function loadMore() {
    currentPageRef.current = currentPageRef.current + 1
    getData()
  }
  // return <h3>{message}</h3>;
  return (
    <div style={{ width: "100vw", height: "100vh", display: "flex", flexDirection: "row", alignContent: 'start', alignItems: "start", justifyContent: "start" }}>
      <div style={{ overflow: "auto", width: "50vw", height: "100vh", background: "green", display: "flex", flexDirection: "column", alignContent: 'center', alignItems: "center", justifyContent: "start" }}>
        {dataListRef.current.map((item, index) => {
          if (item.type == 'dataItem') {
            return (<div key={`parent-${index}`} style={{ width: "100%", background: "gray", marginBottom: "10px", display: "flex", flexDirection: "row", alignContent: 'start', alignItems: "center", justifyContent: "start" }}>
              <div key={`paren-data-${index}`} style={{ textAlign: "center", alignContent: 'center', alignItems: "center", justifyContent: "center" }}>{item?.lottery_date}</div>
              <div key={`parent-child-container-${index}`} style={{ flex: "1", marginLeft: "20px", height: "60px", display: "flex", flexDirection: "row", alignContent: 'start', alignItems: "center", justifyContent: "start" }}>

                {item.numberList.map((childItem, childIndex) => (
                  <div key={`child-${childIndex}`} style={{ marginLeft: childIndex == 0 ? "0px" : "20px", borderRadius: "20px", width: "40px", height: "40px", background: childIndex == 5 || childIndex == 6 ? "blue" : "red", textAlign: "center", alignContent: 'center', alignItems: "center", justifyContent: "center" }}>{childItem}</div>
                ))}
              </div>
            </div>)
          } else {
            return <div onClick={loadMore} style={{ width: "100%", height: "60px", background: "white", textAlign: 'center', paddingTop: "20px", paddingBottom: "20px", alignContent: 'center', alignItems: 'center', justifyContent: "center" }}>{item.text}</div>
          }
        })}
      </div>
      <div style={{ width: "50vw", height: "100vh", background: "red", display: "flex", flexDirection: "column", alignContent: 'start', alignItems: "start", justifyContent: "start" }}>
        <div style={{ overflow: "auto", width: "100%", height: "100%" }}>{message}</div>
      </div>

    </div>
  )
}

export default Home;