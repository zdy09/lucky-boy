const people = [
  '凯瑟琳·约翰逊: 数学家',
  '马里奥·莫利纳: 化学家',
  '穆罕默德·阿卜杜勒·萨拉姆: 物理学家',
  '珀西·莱温·朱利亚: 化学家',
  '苏布拉马尼扬·钱德拉塞卡: 天体物理学家',
];

function About() {
  const outerListItems = ['Item 1', 'Item 2', 'Item 3'];
  const innerListItems = ['Subitem 1', 'Subitem 2', 'Subitem 3'];

  return (
    <ul>
      {outerListItems.map((item, index) => (
        <li key={`outer-${index}`}>
          {item}
          <ul>
            {innerListItems.map((subItem, subIndex) => (
              <li key={`inner-${subIndex}`}>{subItem}</li>
            ))}
          </ul>
        </li>
      ))}
    </ul>
  );
}
export default About